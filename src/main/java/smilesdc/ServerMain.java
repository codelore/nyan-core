package smilesdc;

/**
 * @Autor Smilesdc.
 */

import java.util.logging.Logger;
import net.minecraft.workbench.Workbench;
import net.minecraft.workbench.WorkbenchImpl;
import net.minecraft.workbench.server.Server;
import smilesdc.server.MinecraftServer;


public class ServerMain extends WorkbenchImpl {

    /**
     * Сервер.
     */
    private MinecraftServer server;
    
    /*
     * Логгер.
     */
    public static final Logger logger = Logger.getLogger("Server");
    /**
     * Главный метод.
     *
     * @param args Командная строка.
     */
    public static void main(String[] args) {
        ServerMain main = new ServerMain();
        Workbench.setInstance(main);
        main.startServer(args);
    }

    /**
     * Старт сервера.
     */
    private void startServer(String[] args) {
        server = new MinecraftServer();
        server.start();
    }

    @Override
    public String getImplementationName() {
        return "Server";
    }

    @Override
    public String getImplementationVersion() {
        return "0.0.1-SNAPSHOT";
    }
    @Override
    public Server getServer() {
        return server;
    }
}
