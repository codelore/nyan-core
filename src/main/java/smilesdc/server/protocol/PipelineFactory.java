/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server.protocol;

/**
 *
 * @author Smile
 */
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.StaticChannelPipeline;
import smilesdc.server.MinecraftServer;

/**
 *PipelineFactory для протокола Minecraft.
 */
public class PipelineFactory implements ChannelPipelineFactory {

    /**
     * Сервер.
     */
    private final MinecraftServer server;

    /**
     * Создаем новый PipelineFactory для @param server.
     * @param server server.
     */
    public PipelineFactory(MinecraftServer server) {
        this.server = server;
    }
/*
 * Берем Pipeline.
 */
    public ChannelPipeline getPipeline() throws Exception {
        return new StaticChannelPipeline(
            new Decoder(),//Декодер
            new Encoder(),//Енкодер
            new Handler(server)//Хендлер
        );
    }
}