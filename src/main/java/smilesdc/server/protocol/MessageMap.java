package smilesdc.server.protocol;


import java.util.HashMap;
import java.util.Map;
import smilesdc.server.protocol.codek.MessageCodec;
import smilesdc.server.protocol.handler.MessageHandler;
import smilesdc.server.protocol.message.Message;

/**
 * Обеспечение отображения сообщений своим кодекам и обработчикам.
 */
public class MessageMap {

    /**
     * Таблица из протокола опкодов в кодеки.
     */
    private static final MessageCodec<?>[] opcodeTable = new MessageCodec<?>[256];

    /**
     * Карта сообщения классов для своих кодеков.
     */
    private static final Map<Class<? extends Message>, MessageCodec<?>> codecs = new HashMap<Class<? extends Message>, MessageCodec<?>>();

    /**
     * Карта сообщений классов для своих методов.
     */
    private static final Map<Class<? extends Message>, MessageHandler<?>> handlers = new HashMap<Class<? extends Message>, MessageHandler<?>>();

    private MessageMap() {
        // Щит, не знаю, что тут.
    }
//Далее все стыренно.
    private static <T extends Message, C extends MessageCodec<T>> void bind(Class<T> messageClass, Class<C> codecClass) throws InstantiationException, IllegalAccessException {
        MessageCodec<T> codec = codecClass.newInstance();
        opcodeTable[codec.getOpcode()] = codec;
        codecs.put(messageClass, codec);
    }

    private static <T extends Message, C extends MessageCodec<T>, H extends MessageHandler<T>> void bind(Class<T> messageClass, Class<C> codecClass, Class<H> handlerClass) throws InstantiationException, IllegalAccessException {
        bind(messageClass, codecClass);
        MessageHandler<T> handler = handlerClass.newInstance();
        handlers.put(messageClass, handler);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Message> MessageHandler<T> getHandler(Class<T> clazz) {
        return (MessageHandler<T>) handlers.get(clazz);
    }

    public static MessageCodec<?> getCodec(int opcode) {
        return opcodeTable[opcode];
    }

    @SuppressWarnings("unchecked")
    public static <T extends Message> MessageCodec<T> getCodec(Class<T> clazz) {
        return (MessageCodec<T>) codecs.get(clazz);
    }

    static {
        try {
            // TODO: obviously this isn't real, put some real ones here
            bind(Message.class, MessageCodec.class, MessageHandler.class);
        }
        catch (Exception ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

}