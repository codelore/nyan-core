/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server.protocol;

/**
 *
 * @author Smile
 */
import smilesdc.server.protocol.codek.MessageCodec;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.replay.ReplayingDecoder;
import org.jboss.netty.handler.codec.replay.VoidEnum;

import java.io.IOException;

/**
 * ReplayingDecoder, который декодирует ChannelBuffers в сообщения.
 */
public class Decoder extends ReplayingDecoder<VoidEnum> {

    private int previousOpcode = -1;

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel c, ChannelBuffer buf, VoidEnum state) throws Exception {
        int opcode = buf.readUnsignedByte();

        MessageCodec<?> codec = MessageMap.getCodec(opcode);
        if (codec == null) {
            throw new IOException("Unknown operation code: " + opcode + " (previous opcode: " + previousOpcode + ").");
        }

        previousOpcode = opcode;

        return codec.decode(buf);
    }

}