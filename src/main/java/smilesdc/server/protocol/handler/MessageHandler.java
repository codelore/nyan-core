package smilesdc.server.protocol.handler;


import smilesdc.server.players.MinecraftPlayer;
import smilesdc.server.protocol.Session;
import smilesdc.server.protocol.message.Message;

public abstract class MessageHandler<T extends Message> {

    public abstract void handle(Session session, MinecraftPlayer player, T message);

}