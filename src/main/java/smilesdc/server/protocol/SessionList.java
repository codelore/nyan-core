package smilesdc.server.protocol;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public final class SessionList {

   
    private final ConcurrentMap<Session, Boolean> sessions = new ConcurrentHashMap<Session, Boolean>();

    public void update() {
        for (Session session : sessions.keySet()) {
            session.update();
        }
    }

    public void add(Session session) {
        sessions.put(session, true);
    }

    public void remove(Session session) {
        sessions.remove(session);
    }

}