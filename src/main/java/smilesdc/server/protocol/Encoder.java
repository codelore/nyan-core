/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server.protocol;

/**
 *
 * @author Smile
 */

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

import java.io.IOException;
import smilesdc.server.protocol.codek.MessageCodec;
import smilesdc.server.protocol.message.Message;

/**
 *OneToOneEncode, который кодирует Minecraft сообщения в ChannelBuffers.
 */
public class Encoder extends OneToOneEncoder {

    @SuppressWarnings("unchecked")
    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel c, Object msg) throws Exception {
        if (msg instanceof Message) {
            Message message = (Message) msg; // сообщение

            Class<? extends Message> clas = message.getClass();
            MessageCodec<Message> codec = (MessageCodec<Message>) MessageMap.getCodec(clas);
            if (codec == null) {
                throw new IOException("Unknown message type: " + clas + ".");
            }

            ChannelBuffer opcodeBuffer = ChannelBuffers.buffer(1);
            opcodeBuffer.writeByte(codec.getOpcode());

            return ChannelBuffers.wrappedBuffer(opcodeBuffer, codec.encode(message));
        }
        return msg;
    }

}
