package smilesdc.server.protocol;

import smilesdc.server.MinecraftServer;
import org.jboss.netty.channel.Channel;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;
import smilesdc.server.players.MinecraftPlayer;
import smilesdc.server.protocol.handler.MessageHandler;
import smilesdc.server.protocol.message.Message;

/**
 * Сессия.
 */
public final class Session {

    /**
     *Кол-во тиков, после чего -> отключение.
     */
    private static final int TIMEOUT_TICKS = 300;

    /**
     * Состояние соединения.
     */
    public enum State {
        
        //Стыренно.

        /**
         * In the exchange handshake state, the server is waiting for the client
         * to send its initial handshake packet.
         */
        EXCHANGE_HANDSHAKE,

        /**
         * In the exchange identification state, the server is waiting for the
         * client to send its identification packet.
         */
        EXCHANGE_IDENTIFICATION,

        /**
         * In the game state the session has an associated player.
         */
        GAME
    }
//Стыренно конец.
    /**
     * Сервер.
     */
    
    private final MinecraftServer server;

    /**
     * Рандомное для сессии.
     */
    private final Random random = new Random();

    /**
     * Канал должен быть соединен с сессией.
     */
    private final Channel channel;

 
    private final Queue<Message> messageQueue = new ArrayDeque<Message>();

  
    private int timeoutCounter = 0;

  
    private State state = State.EXCHANGE_HANDSHAKE;

    private MinecraftPlayer player;

   
    private String sessionId = Long.toString(random.nextLong(), 16).trim();

    
    private int pingMessageId;

    
    public Session(MinecraftServer server, Channel channel) {
        this.server = server;
        this.channel = channel;
    }

   
    public State getState() {
        return state;
    }

   
    public void setState(State state) {
        this.state = state;
    }

    public MinecraftPlayer getPlayer() {
        return player;
    }

    
    public void setPlayer(MinecraftPlayer player) {
        if (this.player != null)
            throw new IllegalStateException();

        this.player = player;
    }

    @SuppressWarnings("unchecked")
    public void update() {
        timeoutCounter++;

        Message message;
        while ((message = messageQueue.poll()) != null) {
            MessageHandler<Message> handler = (MessageHandler<Message>) MessageMap.getHandler(message.getClass());
            if (handler != null) {
                handler.handle(this, player, message);
            }
            timeoutCounter = 0;
        }

        if (timeoutCounter >= TIMEOUT_TICKS) {
            if (pingMessageId == 0) {
                pingMessageId = new Random().nextInt();
                //send(new PingMessage(pingMessageId));
                timeoutCounter = 0;
            } else {
                disconnect("Timed out");
            }
        }
    }

    
    public void send(Message message) {
        channel.write(message);
    }

    
    public void disconnect(String reason) {
        dispose();
    
        //channel.write(new KickMessage(reason)).addListener(ChannelFutureListener.CLOSE);
        channel.close();
    }

    public MinecraftServer getServer() {
        return server;
    }
    
    public InetSocketAddress getAddress() {
        SocketAddress addr = channel.getRemoteAddress();
        if (addr instanceof InetSocketAddress) {
            return (InetSocketAddress) addr;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "Session[address=" + channel.getRemoteAddress() + "]";
    }

    <T extends Message> void messageReceived(T message) {
        messageQueue.add(message);
    }

    void dispose() {
        if (player != null) {            
            
            player = null; // in case we are disposed twice
        }
    }

    public String getSessionId() {
        return sessionId;
    }

    public int getPingMessageId() {
        return pingMessageId;
    }

    public void pong() {
        timeoutCounter = 0;
        pingMessageId = 0;
    }

}