package smilesdc.server.protocol.codek;

import org.jboss.netty.buffer.ChannelBuffer;

import java.io.IOException;
import smilesdc.server.protocol.message.Message;

public abstract class MessageCodec<T extends Message> {

    private final int opcode;

    public MessageCodec(int opcode) {
        this.opcode = opcode;
    }

    public final int getOpcode() {
        return opcode;
    }

    public abstract ChannelBuffer encode(T message) throws IOException;

    public abstract T decode(ChannelBuffer buffer) throws IOException;

}