package smilesdc.server.protocol.message;

public abstract class Message {

    public abstract String toString();

}