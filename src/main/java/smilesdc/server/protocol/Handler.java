/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server.protocol;

/**
 *
 * @author Smile
 */
import smilesdc.server.MinecraftServer;
import org.jboss.netty.channel.*;

import java.util.logging.Level;
import smilesdc.server.protocol.message.Message;

/**
 * SimpleChannelUpstreamHandler, который обрабатывает входящие события сети.
 */
public class Handler extends SimpleChannelUpstreamHandler {

    /**
     * Сервер
     */
    private final MinecraftServer server;

    /**
     * Создаем новый обработчик событий сети.
     * @param server сервер.
     */
    public Handler(MinecraftServer server) {
        this.server = server;
    }
// Если соединение установленно.
    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
        Channel channel = e.getChannel(); //сам канал

        Session session = new Session(server, channel); // сессия.
        ctx.setAttachment(session);// добавляем параметр сессия.
        server.addChannel(channel, session);// добавляем канал с сессией.

        server.getLogger().info("Channel connected: " + channel + ".");//сообщаем, о том, что подключились.
    }
// отключение
    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
        Channel channel = e.getChannel();
        Session session = (Session) ctx.getAttachment();
        server.removeChannel(channel, session);
        session.dispose();

        server.getLogger().info("Channel disconnected: " + channel + ".");
    }
//Сообщения.
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        Session session = (Session) ctx.getAttachment();
        session.messageReceived((Message) e.getMessage());
    }
//Ошибка в подключении.
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        Channel channel = e.getChannel();
        if (channel.isOpen()) {
            server.getLogger().log(Level.WARNING, "Exception caught, closing channel: " + channel + "...", e.getCause());//Выводим ошибку.
            server.getLogger().log(Level.INFO,"Oh shi~");
            channel.close();
        }
    }

}