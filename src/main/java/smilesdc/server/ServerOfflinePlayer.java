/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server;

/**
 *
 * @author Smile
 */
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.Location;

/**
 * Represents a player which is not connected to the server.
 */
@SerializableAs("Player")
public class ServerOfflinePlayer {

    private final MinecraftServer server;
    private final String name;

    public ServerOfflinePlayer(MinecraftServer server, String name) {
        this.server = server;
        this.name = name;
    }

    public boolean isOnline() {
        return false;
    }

    public String getName() {
        return name;
    }

    public boolean isBanned() {
        return server.getBanList().isBanned(name);
    }

    public void setBanned(boolean banned) {
        server.getBanList().isBanned(name);
    }

/*
    public void setWhitelisted(boolean value) {
        if (value) {
            server.getWhitelist().add(name);
        } else {
            server.getWhitelist().remove(name);
        }
    }

    public Player getPlayer() {
        return server.getPlayerExact(name);
    }

    public boolean isOp() {
        return server.getOpsList().contains(name);
    }

    public void setOp(boolean value) {
        if (value) {
            server.getOpsList().add(name);
        } else {
            server.getOpsList().remove(name);
        }
    }
*/
    public Map<String, Object> serialize() {
        Map<String, Object> ret = new HashMap<String, Object>();

        ret.put("name", name);
        return ret;
    }

    public static OfflinePlayer deserialize(Map<String, Object> val) {
        return Bukkit.getServer().getOfflinePlayer(val.get("name").toString());
    }

    public long getFirstPlayed() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public long getLastPlayed() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean hasPlayedBefore() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Location getBedSpawnLocation() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

