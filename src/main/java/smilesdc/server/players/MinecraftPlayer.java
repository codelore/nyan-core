/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server.players;

/**
 *
 * @author Smile
 */
import net.minecraft.workbench.server.entities.Entity;
import net.minecraft.workbench.server.players.Player;

import java.util.UUID;

/**
 * Коннект игрока.
 */
public class MinecraftPlayer implements Player {

    /**
     * UUID игрока.
     */
    private final UUID uuid;

    /**
     * Имя игрока.
     */
    private final String username;

    /**
     * Дисплейнейм игрока.
     */
    private String displayName;

    /**
     * Entity игрока.
     */
    private Entity playerEntity;
//Метод, обработки.
    public MinecraftPlayer(UUID uuid, String username, Entity playerEntity) {
        this.uuid = uuid;
        this.username = username;
        this.displayName = username;
        this.playerEntity = playerEntity;
    }
//Конец.
    
    //Метод взятия UUID.
    public UUID getUUID() {
        return uuid;
    }
//Метод взятия имени игрока.
    public String getUsername() {
        return username;
    }
//Метод взятия диплейнейма.
    public String getDisplayName() {
        return displayName;
    }
//Entity соответственно.
    public Entity getControlledEntity() {
        return playerEntity;
    }

}
