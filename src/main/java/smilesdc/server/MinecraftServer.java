/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smilesdc.server;

/**
 *
 * @author Smile
 */
import java.io.File;
import java.io.IOException;
import net.minecraft.workbench.server.Server;
import net.minecraft.workbench.server.players.BanList;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
//==============================================================================
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.yaml.snakeyaml.error.YAMLException;
import smilesdc.server.io.StorageQueue;
import smilesdc.server.protocol.PipelineFactory;
import smilesdc.server.protocol.Session;
import smilesdc.server.protocol.SessionList;

/**
 * Главный серверный класс.
 */
public class MinecraftServer implements Server {

    /**
     * Бан-лист.
     */
    private BanList banList = null;

    /**
     * Бутстрап нужен для инициализирования Netty.
     */
    
    private final ServerBootstrap bootstrap = new ServerBootstrap();

    /**
     * A group containing all of the Channels the server has open.
     */
    private final ChannelGroup channelGroup = new DefaultChannelGroup();

    /**
     * The logger for server output.
     */
    private final Logger logger = Logger.getLogger("Server");

    /**
     * The list of all connected clients.
     */
    private final SessionList sessionList = new SessionList();
    
    /*
     * Конфигурационный файл
     */
    private static final File configDir = new File("config");
    
    /*
     * Файл создания Server.yml
     */
    
    private static final File configFile = new File(configDir, "server.yml");
    
    /*
     * YAML файл. Взято из Bukkit().
     */
    
    private static final YamlConfiguration config = new YamlConfiguration();
    
    /*
     * Версия протокола.. Временно не работает.
     */
    
    public static final int PROTOCOL_VERSION = 22;
    
    public static final StorageQueue storeQueue = new StorageQueue();

    // =========
    // Internals
    // =========

    /**
     * Создаем новый MinecraftServer..
     */
    public MinecraftServer() {
        ExecutorService executor = Executors.newCachedThreadPool();
        bootstrap.setFactory(new NioServerSocketChannelFactory(executor, executor));
        bootstrap.setPipelineFactory(new PipelineFactory(this));
    }

    /**
     * Start the server.
     */
    public void start() {
        try {
            storeQueue.start();

            if (!configDir.exists() || !configDir.isDirectory())
                configDir.mkdirs();
            loadConfiguration();
            config.options().indent(4);
          //ConfigurationSerialization.registerClass(ServerOfflinePlayer.class);
            int port = config.getInt("server.port", 25565);            
        // TODO: read us a configuration
        channelGroup.add(bootstrap.bind(new InetSocketAddress(port)));
        Channel max_value = channelGroup.find(Integer.SIZE);
        logger.info("\nStart the Server!");
        logger.info("Debug info: \n");
        logger.info("Channel size: " + max_value);
        logger.info("Channel Group Name: "+ channelGroup.getName());
    } catch (Throwable t) {
            logger.log(Level.SEVERE, "Error during server startup.", t);
    }
    }

    /**
     * Gracefully stop the server.
     */
    public void shutdown() {
        channelGroup.close();
        bootstrap.getFactory().releaseExternalResources();
    }

    /**
     * Register a channel and session pair.
     * @param channel The channel to register.
     * @param session The session to register.
     */
    public void addChannel(Channel channel, Session session) {
        channelGroup.add(channel);
        sessionList.add(session);
    }

    /**
     * Remove a channel and session pair.
     * @param channel The channel to remove.
     * @param session The session to remove.
     */
    public void removeChannel(Channel channel, Session session) {
        channelGroup.remove(channel);
        sessionList.remove(session);
    }

    /**
     * Get the server's logger.
     * @return The logger.
     */
    public Logger getLogger() {
        return logger;
    }

    // ==========
    // Public API
    // ==========

    public BanList getBanList() {
        return banList;
    }

    public void setBanList(BanList banList) {
        this.banList = banList;
    }
    public boolean loadConfiguration() {
        try {
            if (!configDir.exists() || !configDir.isDirectory()) {
                configDir.mkdirs();
            }
            if (!configFile.exists()) {
                configFile.createNewFile();
            }
            config.load(configFile);
            return true;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Cannot load " + configFile, ex);
        } catch (InvalidConfigurationException ex) {
            if (ex.getCause() instanceof YAMLException) {
                logger.severe("Config file " + configFile + " isn't valid! " + ex.getCause());
            } else if ((ex.getCause() == null) || (ex.getCause() instanceof ClassCastException)) {
                logger.severe("Config file " + configFile + " isn't valid!");
            } else {
                logger.log(Level.SEVERE, "Cannot load " + configFile + ": " + ex.getCause().getClass(), ex);
            }
        }
        return false;
    }
}
